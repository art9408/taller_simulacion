
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.filechooser.FileNameExtensionFilter;
import net.miginfocom.swing.MigLayout;

public class Main extends JFrame implements ActionListener {

    private static int[] primer_linea;
    private static HashMap<Integer, List<Integer>> distancias;
    private static List<Boolean> parqueaderos;
    private static List<List<Integer>> clientes;
    private static List<List<Integer>> out;

    // resultados del taller
    private static List<List<Integer>> salida;
    private static List<List<Integer>> entrada;

    //variables de la vista
    static JFrame frame;
    static JPanel pSalida, pEntrada;
    static JButton btnGenerarSalida, btnGuardarSalida, btnGenerarEntrada, btnGuardarEntrada;
    static JTextField txtArchivo, txtL, txtP, txtE, txtK;
    static JLabel lbL, lbP, lbE, lbK;
    static JTextArea txtAreaEntrada, txtAreaEntradaSalida, txtAreaEntradaEntrada;

    public void vista() {
        frame = new JFrame();
        frame.setLayout(new MigLayout());
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setPreferredSize(new Dimension(700, 400));
        frame.setResizable(false);
        pSalida = new JPanel(new MigLayout());
        frame.add(pSalida, "width max(100%, 100%) ");
        pEntrada = new JPanel(new MigLayout());
        frame.add(pEntrada, "width max(100%, 100%) ");

        //componentes de pSalida
        btnGenerarSalida = new JButton("Generar Salida");
        pSalida.add(btnGenerarSalida, "span 2");
        txtArchivo = new JTextField();
        txtArchivo.enable(false);
        pSalida.add(txtArchivo, "wrap, width max(30%, 30%)");
        txtAreaEntrada = new JTextArea();
        txtAreaEntrada.enable(false);
        pSalida.add(txtAreaEntrada, " width max(40%, 40%)");
        txtAreaEntradaSalida = new JTextArea();
        txtAreaEntradaSalida.enable(false);
        pSalida.add(txtAreaEntradaSalida, "span, wrap, width max(40%, 40%)");
        btnGuardarSalida = new JButton("Generar Archivo");
        pSalida.add(btnGuardarSalida, "span");

        //componentes de entrada        
        lbL = new JLabel("L: ");
        pEntrada.add(lbL, "right");
        txtL = new JTextField();
        pEntrada.add(txtL, "left,wrap, width max(30%, 30%)");
        lbP = new JLabel("P: ");
        pEntrada.add(lbP, "right");
        txtP = new JTextField();
        pEntrada.add(txtP, "left,wrap, width max(30%, 30%)");
        lbE = new JLabel("E: ");
        pEntrada.add(lbE, "right");
        txtE = new JTextField();
        pEntrada.add(txtE, "left,wrap, width max(30%, 30%)");
        lbK = new JLabel("K: ");
        pEntrada.add(lbK, "right");
        txtK = new JTextField();
        pEntrada.add(txtK, "left,wrap, width max(30%, 30%)");

        btnGenerarEntrada = new JButton("Generar Entrada");
        pEntrada.add(btnGenerarEntrada, "wrap,span 2");

        btnGuardarEntrada = new JButton("Generar Archivo");
        pEntrada.add(btnGuardarEntrada, "span 2");

        txtAreaEntradaEntrada = new JTextArea();
        txtAreaEntradaEntrada.enable(false);
        pEntrada.add(txtAreaEntradaEntrada, "east, width max(40%, 40%)");
        /**
         * acciones listener *
         */
        btnGenerarSalida.addActionListener(this);
        btnGuardarSalida.addActionListener(this);
        btnGenerarEntrada.addActionListener(this);
        btnGuardarEntrada.addActionListener(this);

        frame.pack();

    }

    static public File cargar_archivo() {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        //System.out.print(System.getProperty("user.dir"));
        JFileChooser selectorArchivos = new JFileChooser(System.getProperty("user.dir") + "/visitas");
        selectorArchivos.setFileSelectionMode(JFileChooser.FILES_ONLY);
        FileNameExtensionFilter filter = new FileNameExtensionFilter("*.TXT", "txt");
        selectorArchivos.addChoosableFileFilter(filter);
        selectorArchivos.setAcceptAllFileFilterUsed(false);
        int seleccion = selectorArchivos.showOpenDialog(null);
        if (seleccion == JFileChooser.APPROVE_OPTION) {
            File fichero = selectorArchivos.getSelectedFile();
            txtArchivo.setText(fichero.getName());
            return fichero;
        }
        return null;
    }

    public void guardar_archivo(String[] text) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            JFileChooser file = new JFileChooser(System.getProperty("user.dir") + "/visitas");
            file.showSaveDialog(this);
            File guarda = file.getSelectedFile();
            BufferedWriter br = null;
            if (guarda != null) {
                FileWriter fw = new FileWriter(guarda + ".txt");
                br = new BufferedWriter(fw);
                for (String s : text) {
                    br.append(s);
                    br.newLine();
                }
                br.close();
                fw.close();
                JOptionPane.showMessageDialog(null,
                        "El archivo se a guardado Exitosamente",
                        "Información", JOptionPane.INFORMATION_MESSAGE);
            }
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null,
                    "Su archivo no se ha guardado",
                    "Advertencia", JOptionPane.WARNING_MESSAGE);
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /////////////////// Generar salida ///////////////////////    
    static public void leer_archivo(File f) {
        FileReader fr = null;
        BufferedReader br = null;
        String entrada_txt = "";
        try {
            fr = new FileReader(f);
            br = new BufferedReader(fr);
            salida = new ArrayList<>();
            out = new ArrayList<>();
            distancias = new HashMap<>();
            /**
             * L (número de locales) - P (número de sitios de parqueo) - K (el
             * parámetro mencionado arriba) *
             */
            primer_linea = Arrays
                    .stream(br.readLine().split(" ")) //convirtiendo la linea de texto en array
                    .mapToInt(Integer::parseInt) // parseo a enteros 
                    .toArray(); // convirtiendo a array

            entrada_txt += Arrays.toString(primer_linea) + "\n";

            /**
             * filas = locales | columnas = parqueaderos | valor = distancia
             *
             *
             */
            for (int i = 0; i < primer_linea[0]; i++) {
                List l = Arrays
                        .stream(br.readLine().split(" "))//convirtiendo la linea de texto en array
                        .mapToInt(Integer::parseInt)// parseo a enteros 
                        .boxed().collect(Collectors.toList());// convirtiendo a list
                entrada_txt += l.toString() + "\n";
                distancias.put(i, l);
            }

            /**
             * T es el tiempo de llegada del cliente - D es el tiempo que
             * tardará - # nl es el número de locales - locales visitatos
             * l1,l2,l3 etc *
             */
            clientes = br.lines()
                    .map(linea -> Arrays.stream(linea.split(" "))//convirtiendo la linea de texto en array
                    .mapToInt(Integer::parseInt)// parseo a enteros 
                    .boxed().collect(Collectors.toList()))// convirtiendo a list
                    .collect(Collectors.toList());

            entrada_txt = clientes.stream().map((a) -> a.toString() + "\n").reduce(entrada_txt, String::concat);
            txtAreaEntrada.setText(entrada_txt.replaceAll(",", " ").replaceAll("\\[", "").replaceAll("]", ""));
            br.close();
            fr.close();
        } catch (IOException | NumberFormatException e) {
            JOptionPane.showMessageDialog(null, "Error al cargar el archivo, valide el archivo de texto");
        }
    }

    static public void generarSalida() {
        parqueaderos = Stream.generate(() -> false).limit(primer_linea[1]).collect(Collectors.toList());
        //System.out.println("parqueaderos -> " + parqueaderos.toString());
        // tiempo de llegada, tiempo de salida, # de locales, locales ...
        int id = 1;
        for (List<Integer> f : clientes) {
            liberarParqueadero(f.get(0));
            int numero_parqueaderos_disponibles = (int) parqueaderos.stream().filter(x -> x == false).count();
            if (numero_parqueaderos_disponibles > 0) {
                int parqueadero = numero_parqueaderos_disponibles > 1 ? mejorParqueadero(f) : parqueaderos.indexOf(false);
                int distancia = calcularDistancia(parqueadero, f.subList(3, f.size()));
                changeStateParquedero(parqueadero);
                newOutput(newInput(id, parqueadero, f.get(0), distancia), f.get(1));
                id++;
            } else {
                newInput(-1, -1, f.get(0), -1);
            }
            //System.out.println(numero_parqueaderos_disponibles);

        }
        salida = salida.stream()
                .sorted((x, y) -> x.get(1) - y.get(1))
                .collect(Collectors.toList());
        txtAreaEntradaSalida.setText(String.join("\n", salidaEnTexto()));
    }

    static public int mejorParqueadero(List<Integer> cliente) {
        int pos_parqueadero;
        int menor_distancia;
        List<Integer> distancia_local = new ArrayList<>();
        List<Integer> orden;
        if (parqueaderos.stream().filter(x -> x == false).count() < 2) {
            return parqueaderos.indexOf(false);
        }
        List<List<Integer>> distancias_cliente = cliente.subList(3, cliente.size())//locales que visita
                .stream()
                .map(x -> distancias.get(x))
                .collect(Collectors.toList());
        for (int i = 0; i < primer_linea[1]; i++) {
            int sum = 0;
            for (int j = 0; j < distancias_cliente.size(); j++) {
                sum += distancias_cliente.get(j).get(i);
            }
            distancia_local.add(parqueaderos.get(i) ? Integer.MAX_VALUE : sum);
        }
        /**
         * orden de distancia segun el local
         */
        //k = primer_linea[2];
        orden = distancia_local
                .stream()
                .sorted((x, y) -> x - y)
                .filter((d) -> d != Integer.MAX_VALUE)
                .mapToInt(d -> d)
                .boxed().collect(Collectors.toList());

        orden = orden.size() >= primer_linea[2] ? orden.subList(0, primer_linea[2]) : orden;

        //System.out.println(distancia_local.toString());
        System.out.println(orden.toString());

        int random = (int) Math.floor(Math.random() * (orden.size()));
        pos_parqueadero = orden.size() == 1 || primer_linea[2] == 1 ? 0 : random;
        menor_distancia = orden.get(pos_parqueadero);
        pos_parqueadero = distancia_local.indexOf(menor_distancia);
        System.out.println("pos-> " + pos_parqueadero + " - " + "distancia-> " + menor_distancia + " - " + "random ->" + random);

        return pos_parqueadero;
    }

    static public void liberarParqueadero(int tiempo) {
        out.stream()
                .sorted((x, y) -> x.get(1) - y.get(1))
                .filter((x) -> x.get(1) <= tiempo)
                .forEach((x) -> {
                    changeStateParquedero(x.get(3));
                    out.remove(x);
                });

    }

    static public int calcularDistancia(int posicion_parqueadero, List<Integer> locales) {
        int total_distancia = 0;
        total_distancia = locales.stream()
                .map((lc) -> distancias.get(lc).get(posicion_parqueadero) * 2)
                .reduce(total_distancia, Integer::sum);
        return total_distancia;
    }

    static public void changeStateParquedero(int posicion) {
        parqueaderos.set(posicion, !parqueaderos.get(posicion));
    }

    static public List<Integer> newInput(int id, int parquedero, int entrada, int distancia_total) {
        List<Integer> input = Arrays.asList(0, entrada, id, parquedero, distancia_total);
        salida.add(input);
        return input;
    }

    static public void newOutput(List<Integer> input, int tiempo) {
        List<Integer> output = Arrays.asList(1, input.get(1) + tiempo, input.get(2), input.get(3));
        out.add(output);
        salida.add(output);
    }

    static public String[] salidaEnTexto() {
        String[] salidaTxt = salida.stream()
                .map((List<Integer> l) -> {
                    String n = "";
                    n += l.get(0) == 0 ? "IN " : "OUT ";
                    n += l.get(2) == -1 ? l.get(1) + " UNAVAILABLE" : l.subList(1, l.size()).toString();
                    n += "\n";
                    return n.replaceAll(",", " ").replaceAll("\\[", "").replaceAll("]", "").trim();
                }).toArray(String[]::new);

        return salidaTxt;
    }

    /////////////////// Generar entrada ///////////////////////
    static public String[] entradaEnTexto() {
        String[] salidaTxt = entrada.stream()
                .map((List<Integer> l) -> {
                    String n = l.toString();
                    return n.replaceAll(",", "").replaceAll("\\[", "").replaceAll("]", "");
                }).toArray(String[]::new);

        return salidaTxt;
    }

    static public List<List<Integer>> generar_entrada() {
        int l, p, e, k;
        int n = 1000000;
        l = Integer.parseInt(txtL.getText());
        p = Integer.parseInt(txtP.getText());
        e = Integer.parseInt(txtE.getText());
        k = Integer.parseInt(txtK.getText());
        final List<Integer> locales = IntStream.rangeClosed(0, l - 1)
                .boxed().collect(Collectors.toList());
        List<List<Integer>> localParqueadero = Stream
                .generate(() -> Stream.generate(() -> (int) Math.floor(Math.random() * n + 1))
                .limit(l).mapToInt(x -> x).boxed().collect(Collectors.toList()))
                .limit(p)
                .collect(Collectors.toList());
        List<List<Integer>> eventos = Stream
                .generate(() -> crearEventos(l, locales))
                .limit(e)
                .sorted((x, y) -> x.get(0) - y.get(0))
                .collect(Collectors.toList());

        //entrada
        entrada = new ArrayList<>();
        entrada.add(0, Arrays.asList(l, p, k));
        entrada.addAll(localParqueadero);
        entrada.addAll(eventos);
        txtAreaEntradaEntrada.setText(String.join("\n", entradaEnTexto()));
        return entrada;
    }

    static public List<Integer> crearEventos(int numero_locales, List<Integer> locales) {
        int n = 1000000;
        List<Integer> e = new ArrayList<>(Arrays.asList(
                (int) Math.floor(Math.random() * n),//tiempo
                (int) Math.floor(Math.random() * n + 1),//tiempo de demora
                (int) Math.floor(Math.random() * numero_locales + 1))); // # de locales
        //algoritmo de Fisher-Yates
        Random r = new Random();
        for (int i = locales.size(); i > 0; i--) {
            int posicion = r.nextInt(i);
            int tmp = locales.get(i - 1);
            locales.set(i - 1, locales.get(posicion));
            locales.set(posicion, tmp);
        }
        e.addAll(locales.subList(0, e.get(2)));
        return e;
    }

    public static void main(String[] args) {
        new Main().vista();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == btnGenerarSalida) {
            leer_archivo(cargar_archivo());
            generarSalida();
        } else if (e.getSource() == btnGuardarSalida) {
            guardar_archivo(salidaEnTexto());
        } else if (e.getSource() == btnGenerarEntrada) {
            generar_entrada();
        } else if (e.getSource() == btnGuardarEntrada) {
            guardar_archivo(entradaEnTexto());
        }

    }

}
